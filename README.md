## Тестовое задание для https://github.com/batchar2/mjpeg-test


тестируем то, что по адресу http://127.0.0.1:8080 воспроизводиться видео


## Использование

- устанавливаем https://sites.google.com/a/chromium.org/chromedriver/home

- устанавлиеваем зависимости
```
pip3 install -r requirements.txt
```
- запускаем тесты
```
pytest --capture=no
```


## Использование через docker

- собираем образ
```
docker build . --tag mjpeg_html_test
```

- запускаем образ
```
docker run --network=host mjpeg_html_test
```
