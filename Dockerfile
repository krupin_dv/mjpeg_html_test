#FROM python:3.6
FROM selenium/node-chrome:3.141.59-vanadium

USER root
RUN apt-get update
RUN apt-get install python3-pip xvfb -y

COPY ./requirements.txt /src/
COPY ./tests /src/
WORKDIR /src/

RUN pip3 install -r requirements.txt

#CMD curl http://127.0.0.1:8080
CMD pytest --capture=no
