import time
import pytest
from pyvirtualdisplay import Display
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


class TestMJPEG:

    def setup(self):
        display = Display(visible=0, size=(800, 800))
        display.start()
        self.host = 'http://127.0.0.1:8080'
        self.driver = webdriver.Chrome(ChromeDriverManager().install())

    def teardown(self):
        self.driver.close()

    @pytest.mark.skip()
    def test_find_mjpeg(self):
        driver = self.driver
        driver.get(self.host)
        cam_xpath = './/img[@src="{host}/mjpeg/camera/1/feed"]'.format(host=self.host)
        elem = driver.find_element_by_xpath(cam_xpath)
        assert elem.is_displayed()
        return elem

    def test_mjpeg_stream(self):
        elem = self.test_find_mjpeg()
        scr1 = elem.screenshot_as_png
        assert len(scr1) > 0
        time.sleep(1/20)  # suppose we have 20fps or more
        scr2 = elem.screenshot_as_png
        assert len(scr2) > 0
        assert not scr1 == scr2
